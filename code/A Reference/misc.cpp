#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#pragma GCC ivdep
//
ios::sync_with_stdio(false);
cin.tie(0); // don't use endl (flush)
//
stringstream ss;
string s;
getline(cin, s);
ss << s;
//
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <map>
#include <queue>
#include <stack>
#include <set>
#include <string>
#include <utility>
#include <vector>
__int128
/*
g++ -Wshadow -Wall -O2 file.cpp -g
g++ -x c++ -Wall -std=c++17 -O2 -static -pipe -o "$DEST" "$@"

gdb a.out

b main : breakpoint at main
r : run
n : next one line (skip function)
s : next one line (into function)
c : continue
q : quit
* bt : list current stack
up : go to the up stack
down : go to the down stack
print var : print variable
*/

//
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
tree_order_statistics_node_update> indexed_set;
indexed_set s;
s.insert(2), s.insert(3), s.insert(7), s.insert(9);
auto x = s.find_by_order(2);
cout << *x <<; // 7
cout << s.order_of_key(7) << "\n"; // 2
cout << s.order_of_key(6) << "\n"; // 2
cout << s.order_of_key(8) << "\n"; // 3