// #include Union-find
int n, m;
ll Kruskal() {
    init(); // Union-Find init
    // edge[i] = {a, b, w}
    sort(edge, edge + m, [](Edge a, Edge b) { return a.w < b.w; }); 
    ll ans = 0; int cnt = 0;
    for(int i = 0; i < m; i++) {
        if(find(edge[i].a) == find(edge[i].b)) continue;
        unite(edge[i].a,edge[i].b);
        ans = ans + edge[i].w;
        cnt++;
    }
    return (cnt == n - 1) ? (ans) : (-1);
}