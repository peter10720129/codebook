typedef pair<int, int> Edge;
vector<Edge> adj[N];
vector<int> Euler_path; 
/* Store Node Number
 * or Store Edge Number */
int deg[N], n;
bool vis_node[N], vis_edge[M];
void DFS(int x) {
    vis_node[x] = 1;
    while(!adj[x].empty()) {
        auto [v, e] = adj[x].back();
        adj[x].pop_back();
        if(!vis_edge[e]) {
            vis_edge[e] = 1;
            DFS(v);
            Euler_path.push_back(v);
        }
    }
}
void Euler() {
    DFS(1);
    Euler_path.push_back(1);
    for(int i = 1; i <= n; i++) {
        if((!vis_node[i] && deg[i] != 0) || deg[i] % 2 != 0) {
            // No Euler Path
        }
    }
}