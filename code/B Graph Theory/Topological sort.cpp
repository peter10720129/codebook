void DFS(int x) {
    vis[x] = 1;
    for(auto i : adj[x]) {
        if(vis[i] == 1) cyclic = 1;
        if(!vis[i]) DFS(i);
    }
    ord.push_back(x);
    vis[x] = 2;
}
void Topological_sort() {
    for(int i = 1;i <= n;i++) {
        if(!vis[i]) DFS(i);
    }
    if(cyclic) printf("IMPOSSIBLE");
    else // reverse ord is order
}
