memset(dis, 0x3f, sizeof(dis));
dis[s] = 0;
queue<int> que; que.push(s);
while(!que.empty()) {
    int x = que.front();
    in_que[x] = 0; que.pop();
    for(auto [v, w] : adj[x]) {
        if(dis[v] > dis[x] + w) {
            dis[v] = dis[x] + w;
            if(!in_que[v]) {
                in_que[v] = 1;
                que.push(v);
            }
        }
    }
}