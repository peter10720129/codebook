int low[N], dfn[N], dfn_to_idx[N];
int SCC_cnt, SCC_id[N];
bool in_s[N];
vector <int> adj[N], s; // s is stack
void DFS(int x) {
    static int ord = 1;
    dfn[x] = low[x] = ord++;
    s.push_back(x);
    in_s[x] = 1;
    for(auto v : adj[x]) {
        if(!dfn[v]) {
            DFS(v);
            low[x] = min(low[x], low[v]);
        }
        else if(in_s[v]) {
            low[x] = min(low[x], dfn[v]);
        }
    }
    if(dfn[x] == low[x]) {
        SCC_cnt++;
        while(s.back() != x) {
            SCC_id[s.back()] = SCC_cnt;
            in_s[s.back()] = 0;
            s.pop_back();
        }
        SCC_id[x] = SCC_cnt;
        in_s[x] = 0;
        s.pop_back();
    }
}
int n, m;
void Tarjan_SCC() {
    SCC_cnt = 0;
    for(int i = 1;i <= n;i++) {
        if(!dfn[i]) DFS(i);
    }
}