int sz[N], par[N], to[N], fr[N], dep[N];
int tree_arr[N], val[N], dfn[N];
void DFS(int x, int p) {
    sz[x] = 1;
    par[x] = p;
    dep[x] = dep[p] + 1;
    to[x] = -1;
    for(auto v : adj[x]) {
        if(v != p) {
            DFS(v, x);
            sz[x] += sz[v];
            if(to[x] == -1 || sz[v] > sz[to[x]]) to[x] = v;
        }
    }
}
void Link(int x, int f) {
    static int ord = 1;
    dfn[x] = ord;
    tree_arr[ord++] = x;
    // and use Data Structure maintain tree_arr
    fr[x] = f;
    if(to[x] != -1) Link(to[x], f);
    for(int v : adj[x]) {
        if(v == par[x] || v == to[x]) continue;
        Link(v, v);
    }
}
vector<pair<int, int>> Query(int u, int v) {
    // return the intervals on the path from u to v
    int fu = fr[u], fv = fr[v];
    vector<pair<int, int>> res;
    while(fu != fv) {
        if(dep[fu] < dep[fv]) {
            swap(fu, fv);
            swap(u, v);
        }
        res.emplace_back(dfn[fu], dfn[u]);
        u = par[fu];
        fu = fr[u];
    }
    if(dep[u] > dep[v]) swap(u, v);
    // u is LCA
    res.emplace_back(dfn[u], dfn[v]);
    return res;
}