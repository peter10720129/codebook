// sum[i] = the sum of the distances from the node i to all other nodes
// cnt[i] = the sum of the distances from the node i to node in subtree
// subtre[i] = size of subtree
ll cnt[N], subtree[N], sum[N];
void toleaf(int x, int par) {
    subtree[x] = 1;
    for(auto i : adj[x]) {
        if(i != par) {
            toleaf(i, x);
            subtree[x] += subtree[i];
            cnt[x] += cnt[i] + subtree[i];
        }
    }
}
void dis_sum(int x, int par) {
    if(par != 0) sum[x] = sum[par] - subtree[x] + (n - subtree[x]);
    else sum[x] = cnt[x];
    for(auto i : adj[x]) if(i != par) dis_sum(i, x);
}