void factorize(ll n, vector<pair<ll, ll>> &fac) {
    for(ll i = 2; i * i <= n; i++) {
        if(n % i == 0) fac.push_back({i, 0});
        while(n % i == 0) {
            fac.back().second++;
            n = n / i;
        }
    }
    if(n > 1) fac.push_back({n,1});
}
// the number of positive integer i <= n, that are relatively prime to n
ll phi(ll n, vector<pair<ll, ll>> fac) {
    factorize(n, fac);
    ll ans = 1;
    for(auto u : fac) {
        ll prime = u.first;
        ll prime_power = u.second;
        ans = ans * (prime - 1) * fp(prime, prime_power - 1);
    }
    return ans;
}