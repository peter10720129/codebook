struct Line {
    ll a, b; // ax + b
    ll val(ll x) { return a * x + b; }
    Line() {}
    Line(ll _a, ll _b) : a(_a), b(_b) {}
};
struct Node {
    int left, right;
    Line L;
    Node *lc, *rc;
};
void build(Node *r, int L, int R) {
    r->left = L, r->right = R;
    r->L = Line(INF, INF);
    if(L == R) return;
    int M = (L + R) / 2;
    build(r->lc = new Node(), L, M);
    build(r->rc = new Node(), M + 1, R);
}
void update(Node *r, Line L) {
    Line left_line, right_line;
    if(L.a > r->L.a) {
        left_line = L;
        right_line = r->L;
    } else {
        left_line = r->L;
        right_line = L;
    }

    int mid = (r->left + r->right) / 2;
    if(left_line.val(mid) < right_line.val(mid)) {
        r->L = left_line;
        if(r->left != r->right) update(r->rc, right_line);
    } else {
        r->L = right_line;
        if(r->left != r->right) update(r->lc, left_line);
    }
}
ll qry(Node *r, int x) {
    if(r->left == r->right)
        return r->L.val(x);
    int mid = (r->left + r->right) / 2;
    if(x <= mid) return min(r->L.val(x), qry(r->lc, x));
    else return min(r->L.val(x), qry(r->rc, x));
}