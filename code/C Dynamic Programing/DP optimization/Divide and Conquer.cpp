/* Divide into k subarrays 
 * The cost of each subarray is the square of the sum of the values in the subarray
 * Find the minimum sum of k costs of subarray */
/* dp[i][k] = minimum value of split pre i-th number into k subarray */
void compute(int j, int left, int right, int left_bound, int right_bound) {
    if(left > right) return;
    int mid = (left + right) / 2;
    pair<ll, int> best; // val, pos
    best = make_pair(INF, -1);
    for(int i = max(j, left_bound);i <= right_bound;i++) {
        /* cost[left][right] = the square of sum of (arr[left ... right]) */
        best = min(best, make_pair(dp[i - 1][j - 1] + cost[i][mid], i));
    }
    dp[mid][j] = best.first;
    compute(j, left, mid - 1, left_bound, best.second);
    compute(j, mid + 1, right, best.second, right_bound);
}

int main() {
    for(int i = 1;i <= n;i++) { dp[i][1] = cost[1][i]; }
    for(int j = 2;j <= k;j++) { compute(j, 1, n, 1, n); }
}