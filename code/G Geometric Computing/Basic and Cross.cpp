#define X real()
#define Y imag()
using P = complex<ll>;
ll cross(P a, P b) { // two vector
    return (conj(a) * b).Y;
}
// two lines f(x) = m * x + c
// the intersection point
// x = (c2 - c1) / (m1 - m2)
// y = (m2c1 - m1c2) / (m2 - m1)