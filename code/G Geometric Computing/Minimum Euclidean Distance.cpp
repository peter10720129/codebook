#define X second // important
#define Y first  // important
typedef pair<ll, ll> Point;
ll dis(Point a, Point b) {
    return (a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y);
}
ll Minimun_Euclidean_Distance(vector<Point> point) { // The point arr and the size
    int n = (int)point.size();
    sort(point.begin(), point.end(), [](Point a, Point b) {
        if(a.X != b.X) return a.X < b.X;
        return a.Y < b.Y; 
    });
    ll ans = (ll)8e18;
    int left = -1;
    set<Point> s;
    for(int right = 0;right < n;right++) {
        ll d = (ll)floor(sqrt(ans));
        while(left + 1 < right && point[left + 1].X < point[right].X - d) {
            auto it = s.find(point[left + 1]);
            assert(it != s.end()); s.erase(it);
            left++;
        }
        // make_pair : First is Y, second is X
        auto lb = s.lower_bound(make_pair(point[right].Y - d, point[right].X - d));
        auto ub = s.upper_bound(make_pair(point[right].Y + d, point[right].X + d));
        for(auto P = lb; P != ub; P++) {
            ans = min(ans, dis(*P, point[right]));
        }
        s.insert(point[right]);
    }
    // distance = sqrt(ans)
    // According to Problem
    return ans;
}