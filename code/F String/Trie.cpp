int last_pos = 0, trie[N][W], cnt[N];
void Insert(string s) {
	int now = 0;
	for(auto c : s) {
		int d = (c - 'a');
		if(trie[now][d] == 0) 
			trie[now][d] = ++last_pos;
		now = trie[now][d];
	}
	cnt[now]++;
}
void Print(int now, string &s) {
	// s occurs cnt[now] times
	for(int i = 0; i < W; i++) {
		if(trie[now][i]) {
			s += ('a' + i);
			Print(trie[now][i], s);
			s.pop_back();
		}
	}
}