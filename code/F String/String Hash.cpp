#define MOD ((ll)1e9 + 7) // 1004535809, 1e9 + 9
#define B (137LL) // 257LL, 31LL (31LL is best) 
ll H[N], Pow_B[N];
void Rolling_Hash(string &s) {
    int n = (int)s.size();
    Pow_B[0] = 1;
    for(int i = 1;i <= n;i++) { // H is 1-index
        H[i] = (H[i - 1] * B + s[i - 1]) % MOD;
        Pow_B[i] = Pow_B[i - 1] * B % MOD;
    }
}
ll get_hash(int left, int right) {
    return (((H[right + 1] - H[left] * Pow_B[right - left + 1] % MOD) + MOD) % MOD);
}
ll all_hash(string &s) {
    ll res = 0;
    for(auto c : s) {
        res = (res * B + c) % MOD;
    }
    return res;
}